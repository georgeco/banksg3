package com.bank.sg.service.Sucursal;

import com.bank.sg.dao.DocumentoPersonal.TipoDocumentoRespository;
import com.bank.sg.dao.Sucursal.TipoSucursalRespository;
import com.bank.sg.entities.Sucursal.Sucursal;
import com.bank.sg.entities.Sucursal.Tipo_Sucursal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TipoSucursalService {
    private TipoSucursalRespository tipoSucursalRespository;

    @Autowired
    public TipoSucursalService(TipoSucursalRespository tipoSucursalRespository) {
        this.tipoSucursalRespository = tipoSucursalRespository;
    }
    public Tipo_Sucursal guardar(Tipo_Sucursal tiposucursal){return tipoSucursalRespository.save(tiposucursal);}
    public Optional<Tipo_Sucursal > buscarId(Integer id){return tipoSucursalRespository.findById(id);}
    public List<Tipo_Sucursal > buscarTodo(Integer id){return tipoSucursalRespository.findAll();}
    public Tipo_Sucursal actualizar(Tipo_Sucursal  tiposucursal){return tipoSucursalRespository.save(tiposucursal);}
    public void eliminarPorId(Integer id){tipoSucursalRespository.deleteById(id);}
}
