package com.bank.sg.service.Transaccion;

import com.bank.sg.dao.Transaccion.TransaccionRespository;
import com.bank.sg.entities.Sucursal.Sucursal;
import com.bank.sg.entities.Transaccion.Transaccion;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class TransaccionService {
    private TransaccionRespository transaccionRespository;

    @Autowired
    public TransaccionService(TransaccionRespository transaccionRespository) {
        this.transaccionRespository = transaccionRespository;
    }

    public Transaccion guardar(Transaccion transaccion){return transaccionRespository.save(transaccion);}
    public Optional<Transaccion> buscarId(Integer id){return transaccionRespository.findById(id);}
    public List<Transaccion> buscarTodo(Integer id){return transaccionRespository.findAll();}
    public Transaccion actualizar(Transaccion transaccion){return transaccionRespository.save(transaccion);}
    public void eliminarPorId(Integer id){transaccionRespository.deleteById(id);}
}
