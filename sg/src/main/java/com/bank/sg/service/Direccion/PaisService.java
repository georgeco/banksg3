package com.bank.sg.service.Direccion;

import com.bank.sg.dao.Direccion.PaisRespository;
import com.bank.sg.entities.Direccion.Direccion;
import com.bank.sg.entities.Direccion.Pais;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PaisService {
    private PaisRespository paisRespository;

    @Autowired
    public PaisService(PaisRespository paisRespository) {
        this.paisRespository = paisRespository;
    }

    public Pais guardar(Pais pais){return paisRespository.save(pais);}
    public Optional<Pais> buscarId(Integer id){return paisRespository.findById(id);}
    public List<Pais> buscarTodo(Integer id){return paisRespository.findAll();}
    public Pais actualizar(Pais pais){return paisRespository.save(pais);}
    public void eliminarPorId(Integer id){paisRespository.deleteById(id);}

}
