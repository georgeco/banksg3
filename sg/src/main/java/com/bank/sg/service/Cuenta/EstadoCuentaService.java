package com.bank.sg.service.Cuenta;

import com.bank.sg.dao.Cuenta.EstadoCuentaRespository;
import com.bank.sg.entities.cuenta.Cuenta;
import com.bank.sg.entities.cuenta.Estado_Cuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EstadoCuentaService {
    private EstadoCuentaRespository estadoCuentaRespository;

    @Autowired
    public EstadoCuentaService(EstadoCuentaRespository estadoCuentaRespository) {
        this.estadoCuentaRespository = estadoCuentaRespository;
    }

    public Estado_Cuenta guardar(Estado_Cuenta estadoCuenta){return estadoCuentaRespository.save(estadoCuenta);}
    public Optional<Estado_Cuenta> buscarId(Integer id){return estadoCuentaRespository.findById(id);}
    public List<Estado_Cuenta> buscarTodo(Integer id){return estadoCuentaRespository.findAll();}
    public Estado_Cuenta actualizar(Estado_Cuenta estadoCuenta){return estadoCuentaRespository.save(estadoCuenta);}
    public void eliminarPorId(Integer id){estadoCuentaRespository.deleteById(id);}

}
