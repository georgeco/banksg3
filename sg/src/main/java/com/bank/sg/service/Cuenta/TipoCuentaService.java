package com.bank.sg.service.Cuenta;

import com.bank.sg.dao.Cuenta.TipoCuentaRespository;
import com.bank.sg.entities.cuenta.Estado_Cuenta;
import com.bank.sg.entities.cuenta.Tipo_cuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TipoCuentaService {

    private TipoCuentaRespository tipoCuentaRespository;

    @Autowired
    public TipoCuentaService(TipoCuentaRespository tipoCuentaRespository) {
        this.tipoCuentaRespository = tipoCuentaRespository;
    }

    public Tipo_cuenta guardar(Tipo_cuenta tipoCuenta){return tipoCuentaRespository.save(tipoCuenta);}
    public Optional<Tipo_cuenta> buscarId(Integer id){return tipoCuentaRespository.findById(id);}
    public List<Tipo_cuenta> buscarTodo(Integer id){return tipoCuentaRespository.findAll();}
    public Tipo_cuenta actualizar(Tipo_cuenta tipoCuenta){return tipoCuentaRespository.save(tipoCuenta);}
    public void eliminarPorId(Integer id){tipoCuentaRespository.deleteById(id);}


}
