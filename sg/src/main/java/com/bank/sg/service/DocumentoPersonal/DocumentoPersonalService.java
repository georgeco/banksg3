package com.bank.sg.service.DocumentoPersonal;

import com.bank.sg.dao.DocumentoPersonal.DocumentoPersonalRespository;
import com.bank.sg.entities.Direccion.Pais;
import com.bank.sg.entities.Documento_personal.Documento_Personal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DocumentoPersonalService {
    private DocumentoPersonalRespository documentoPersonalRespository;

    @Autowired
    public DocumentoPersonalService(DocumentoPersonalRespository documentoPersonalRespository) {
        this.documentoPersonalRespository = documentoPersonalRespository;
    }

    public Documento_Personal guardar(Documento_Personal dp){return documentoPersonalRespository.save(dp);}
    public Optional<Documento_Personal> buscarId(Integer id){return documentoPersonalRespository.findById(id);}
    public List<Documento_Personal> buscarTodo(Integer id){return documentoPersonalRespository.findAll();}
    public Documento_Personal actualizar(Documento_Personal dp){return documentoPersonalRespository.save(dp);}
    public void eliminarPorId(Integer id){documentoPersonalRespository.deleteById(id);}
}
