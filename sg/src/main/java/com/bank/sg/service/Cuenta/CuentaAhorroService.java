package com.bank.sg.service.Cuenta;

import com.bank.sg.dao.Cuenta.CuentaAhorroRespository;
import com.bank.sg.entities.Usuario;
import com.bank.sg.entities.cuenta.Cuenta_Ahorro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CuentaAhorroService {
    private CuentaAhorroRespository cuentaAhorroRespository;

    @Autowired
    public CuentaAhorroService(CuentaAhorroRespository cuentaAhorroRespository) {
        this.cuentaAhorroRespository = cuentaAhorroRespository;
    }

    public Cuenta_Ahorro guardar(Cuenta_Ahorro cat){return cuentaAhorroRespository.save(cat);}
    public Optional<Cuenta_Ahorro> buscarId(Integer id){return cuentaAhorroRespository.findById(id);}
    public List<Cuenta_Ahorro> buscarTodo(Integer id){return cuentaAhorroRespository.findAll();}
    public Cuenta_Ahorro actualizar(Cuenta_Ahorro cat){return cuentaAhorroRespository.save(cat);}
    public void eliminarPorId(Integer id){cuentaAhorroRespository.deleteById(id);}
}
