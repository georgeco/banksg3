package com.bank.sg.service.Direccion;

import com.bank.sg.dao.Direccion.CiudadRespository;
import com.bank.sg.entities.Direccion.Ciudad;
import com.bank.sg.entities.Usuario;
import com.bank.sg.entities.cuenta.Tipo_cuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CiudadService {
    private CiudadRespository ciudadRespository;
    @Autowired
    public CiudadService(CiudadRespository ciudadRespository) {
        this.ciudadRespository = ciudadRespository;
    }

    public Ciudad guardar(Ciudad ciudad){return ciudadRespository.save(ciudad);}
    public Optional<Ciudad> buscarId(Integer id){return ciudadRespository.findById(id);}
    public List<Ciudad> buscarTodo(Integer id){return ciudadRespository.findAll();}
    public Ciudad actualizar(Ciudad ciudad){return ciudadRespository.save(ciudad);}
    public void eliminarPorId(Integer id){ciudadRespository.deleteById(id);}

}
