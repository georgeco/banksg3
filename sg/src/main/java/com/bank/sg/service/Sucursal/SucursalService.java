package com.bank.sg.service.Sucursal;

import com.bank.sg.dao.Sucursal.SucursalRepository;
import com.bank.sg.entities.Sucursal.Modalidad_Sucursal;
import com.bank.sg.entities.Sucursal.Sucursal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SucursalService {

    private SucursalRepository sucursalRepository;

    @Autowired
    public SucursalService(SucursalRepository sucursalRepository) {
        this.sucursalRepository = sucursalRepository;
    }

    public Sucursal guardar(Sucursal sucursal){return sucursalRepository.save(sucursal);}
    public Optional<Sucursal> buscarId(Integer id){return sucursalRepository.findById(id);}
    public List<Sucursal> buscarTodo(Integer id){return sucursalRepository.findAll();}
    public Sucursal actualizar(Sucursal sucursal){return sucursalRepository.save(sucursal);}
    public void eliminarPorId(Integer id){sucursalRepository.deleteById(id);}
}
