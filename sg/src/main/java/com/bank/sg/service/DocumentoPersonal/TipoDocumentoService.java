package com.bank.sg.service.DocumentoPersonal;

import com.bank.sg.dao.DocumentoPersonal.TipoDocumentoRespository;
import com.bank.sg.entities.Documento_personal.Documento_Personal;
import com.bank.sg.entities.Documento_personal.Tipo_documento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TipoDocumentoService {
    private TipoDocumentoRespository tipoDocumentoRespository;

    @Autowired
    public TipoDocumentoService(TipoDocumentoRespository tipoDocumentoRespository) {
        this.tipoDocumentoRespository = tipoDocumentoRespository;
    }

    public Tipo_documento guardar(Tipo_documento tp){return tipoDocumentoRespository.save(tp);}
    public Optional<Tipo_documento> buscarId(Integer id){return tipoDocumentoRespository.findById(id);}
    public List<Tipo_documento> buscarTodo(Integer id){return tipoDocumentoRespository.findAll();}
    public Tipo_documento actualizar(Tipo_documento tp){return tipoDocumentoRespository.save(tp);}
    public void eliminarPorId(Integer id){tipoDocumentoRespository.deleteById(id);}
}
