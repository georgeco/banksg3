package com.bank.sg.service.Transaccion;

import com.bank.sg.dao.Transaccion.TipoTransaccionRespository;
import com.bank.sg.entities.Sucursal.Sucursal;
import com.bank.sg.entities.Transaccion.Tipo_Transaccion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TipoTransaccionService {
    private TipoTransaccionRespository tipoTransaccionRespository;
    @Autowired
    public TipoTransaccionService(TipoTransaccionRespository tipoTransaccionRespository) {
        this.tipoTransaccionRespository = tipoTransaccionRespository;
    }
    public Tipo_Transaccion guardar(Tipo_Transaccion tipoTransaccion){return tipoTransaccionRespository.save(tipoTransaccion);}
    public Optional<Tipo_Transaccion> buscarId(Integer id){return tipoTransaccionRespository.findById(id);}
    public List<Tipo_Transaccion> buscarTodo(Integer id){return tipoTransaccionRespository.findAll();}
    public Tipo_Transaccion actualizar(Tipo_Transaccion tipoTransaccion){return tipoTransaccionRespository.save(tipoTransaccion);}
    public void eliminarPorId(Integer id){tipoTransaccionRespository.deleteById(id);}
}
