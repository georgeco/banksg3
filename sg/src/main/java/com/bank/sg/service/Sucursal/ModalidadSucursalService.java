package com.bank.sg.service.Sucursal;

import com.bank.sg.dao.Sucursal.ModalidadSucursalRespository;
import com.bank.sg.entities.Documento_personal.Tipo_documento;
import com.bank.sg.entities.Sucursal.Modalidad_Sucursal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ModalidadSucursalService {
    private ModalidadSucursalRespository modalidadSucursalRespository;

    @Autowired
    public ModalidadSucursalService(ModalidadSucursalRespository modalidadSucursalRespository) {
        this.modalidadSucursalRespository = modalidadSucursalRespository;
    }

    public Modalidad_Sucursal guardar(Modalidad_Sucursal ms){return modalidadSucursalRespository.save(ms);}
    public Optional<Modalidad_Sucursal> buscarId(Integer id){return modalidadSucursalRespository.findById(id);}
    public List<Modalidad_Sucursal> buscarTodo(Integer id){return modalidadSucursalRespository.findAll();}
    public Modalidad_Sucursal actualizar(Modalidad_Sucursal ms){return modalidadSucursalRespository.save(ms);}
    public void eliminarPorId(Integer id){modalidadSucursalRespository.deleteById(id);}
}
