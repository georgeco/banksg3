package com.bank.sg.service.Cuenta;

import com.bank.sg.dao.Cuenta.CuentaRespository;
import com.bank.sg.entities.cuenta.Cuenta;
import com.bank.sg.entities.cuenta.Cuenta_Ahorro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CuentaService {

    private CuentaRespository cuentaRespository;

    @Autowired
    public CuentaService(CuentaRespository cuentaRespository) {
        this.cuentaRespository = cuentaRespository;
    }

    public Cuenta guardar(Cuenta cuenta){return cuentaRespository.save(cuenta);}
    public Optional<Cuenta> buscarId(Integer id){return cuentaRespository.findById(id);}
    public List<Cuenta> buscarTodo(Integer id){return cuentaRespository.findAll();}
    public Cuenta actualizar(Cuenta cuenta){return cuentaRespository.save(cuenta);}
    public void eliminarPorId(Integer id){cuentaRespository.deleteById(id);}

}
