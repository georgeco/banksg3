package com.bank.sg.service;

import com.bank.sg.dao.UsuarioRespository;
import com.bank.sg.entities.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    private UsuarioRespository usuarioRespository;
    @Autowired
    public UsuarioService(UsuarioRespository usuarioRespository) {
        this.usuarioRespository = usuarioRespository;
    }


    public Usuario guardar(Usuario u){return usuarioRespository.save(u);}
    public Optional<Usuario> buscarId(Integer id){return usuarioRespository.findById(id);}
    public List<Usuario> buscarTodo(Integer id){return usuarioRespository.findAll();}
    public Usuario actualizar(Usuario u){return usuarioRespository.save(u);}
    public void eliminarPorId(Integer id){usuarioRespository.deleteById(id);}

}
