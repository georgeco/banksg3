package com.bank.sg.service.Direccion;

import com.bank.sg.dao.Direccion.DireccionRespository;
import com.bank.sg.entities.Direccion.Direccion;
import com.bank.sg.entities.Usuario;
import com.bank.sg.entities.cuenta.Tipo_cuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DireccionService {

    private DireccionRespository direccionRespository;

    @Autowired
    public DireccionService(DireccionRespository direccionRespository) {
        this.direccionRespository = direccionRespository;
    }

    public Direccion guardar(Direccion direccion){return direccionRespository.save(direccion);}
    public Optional<Direccion> buscarId(Integer id){return direccionRespository.findById(id);}
    public List<Direccion> buscarTodo(Integer id){return direccionRespository.findAll();}
    public Direccion actualizar(Direccion direccion){return direccionRespository.save(direccion);}
    public void eliminarPorId(Integer id){direccionRespository.deleteById(id);}

}
