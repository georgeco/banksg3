package com.bank.sg.service.Cuenta;

import com.bank.sg.dao.Cuenta.CuentaInversionRespository;
import com.bank.sg.entities.cuenta.Cuenta_Ahorro;
import com.bank.sg.entities.cuenta.Cuenta_Inversion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CuentaInversionService {
    private CuentaInversionRespository cuentaInversionRespository;

    @Autowired
    public CuentaInversionService(CuentaInversionRespository cuentaInversionRespository) {
        this.cuentaInversionRespository = cuentaInversionRespository;
    }

    public Cuenta_Inversion guardar(Cuenta_Inversion cdt){return cuentaInversionRespository.save(cdt);}
    public Optional<Cuenta_Inversion> buscarId(Integer id){return cuentaInversionRespository.findById(id);}
    public List<Cuenta_Inversion> buscarTodo(Integer id){return cuentaInversionRespository.findAll();}
    public Cuenta_Inversion actualizar(Cuenta_Inversion cdt){return cuentaInversionRespository.save(cdt);}
    public void eliminarPorId(Integer id){cuentaInversionRespository.deleteById(id);}
}
