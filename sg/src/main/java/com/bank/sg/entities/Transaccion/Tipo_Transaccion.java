package com.bank.sg.entities.Transaccion;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "tipo_transaccion")
public class Tipo_Transaccion {
    @Id
    @SequenceGenerator(name="tipo_transaccion_sequence",sequenceName = "tipo_transaccion_sequence",allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="sequence_generator")
    private Integer id;

}
