package com.bank.sg.entities.Sucursal;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "tipo_sucursal")
@Data
public class Tipo_Sucursal {
    @Id
    @SequenceGenerator(name="tipo_sucursal_sequence",sequenceName = "tipo_sucursal_sequence",allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="sequence_generator")
    private Integer id;

}
