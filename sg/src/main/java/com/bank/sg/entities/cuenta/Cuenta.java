package com.bank.sg.entities.cuenta;


import com.bank.sg.entities.Usuario;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "cuenta")
@Data
public class Cuenta {
    @Id
    @SequenceGenerator(name="cuenta_sequence",sequenceName = "cuenta_sequence",allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="sequence_generator")
    private Integer id;

    @Column(name = "nc")
    private String numeroCuenta;
    @Column(name = "saldo")
    private Double saldo;
    @DateTimeFormat
    private Date fecha_apertura;

    private Double interes_acumulado;

    @ManyToOne()
    @JoinColumn(name = "estadoCuenta")
    private Estado_Cuenta estado_cuenta;

    @ManyToOne()
    private Usuario usuario;

    @OneToOne()
    private Tipo_cuenta tipo_cuenta;
}
