package com.bank.sg.entities.Documento_personal;

import javax.persistence.*;

@Entity
@Table(name = "tipo_documento")
public class Tipo_documento {
    @Id
    @SequenceGenerator(name="tipo_documento_sequence",sequenceName = "tipo_documento_sequence",allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="sequence_generator")
    private Integer id;

    private String tipo;

}
