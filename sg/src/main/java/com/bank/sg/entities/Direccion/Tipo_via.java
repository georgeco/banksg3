package com.bank.sg.entities.Direccion;

import javax.persistence.*;

@Entity
@Table(name = "tipo_via")
public class Tipo_via {
    @Id
    @SequenceGenerator(name="tipo_via_sequence",sequenceName = "tipo_via_sequence",allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="sequence_generator")
    private Integer id;

    private String tipoVia;
}
