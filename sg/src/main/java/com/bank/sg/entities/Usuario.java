package com.bank.sg.entities;

import com.bank.sg.entities.Direccion.Direccion;
import org.hibernate.annotations.Generated;

import javax.persistence.*;

@Entity
@Table(name= "Usuario")
public class Usuario {

    @Id
    @SequenceGenerator(name="usuario_sequence",sequenceName = "usuario_sequence",allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_generator")
    private Integer id;
    private String nombre;
    private String apellido;
    private String Correo;
    private Integer celular;

    @OneToOne()
    private Direccion direccion;


}
