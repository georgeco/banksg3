package com.bank.sg.entities.cuenta;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "estado_cuenta")
@Data
public class Estado_Cuenta {
    @Id
    @SequenceGenerator(name="cuenta_sequence",sequenceName = "cuenta_sequence",allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="sequence_generator")
    private Integer id;

    private String estado;
}
