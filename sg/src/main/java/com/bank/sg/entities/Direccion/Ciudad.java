package com.bank.sg.entities.Direccion;

import javax.persistence.*;

@Entity
@Table(name = "ciudad")
public class Ciudad {
    @Id
    @SequenceGenerator(name="ciudad_sequence",sequenceName = "ciudad_sequence",allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="sequence_generator")
    private Integer id;

    //@OneToOne(mappedBy = "ciudad")
    //private Pais pais;

    private String ciudad;
}
