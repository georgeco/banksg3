package com.bank.sg.entities.Usuario_Autentication;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "estado_usuario")
public class Estado_Usuario {
    @Id
    @SequenceGenerator(name="estado_usuario_sequence",sequenceName = "estado_usuario_sequence",allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="sequence_generator")
    private Integer id;

}
