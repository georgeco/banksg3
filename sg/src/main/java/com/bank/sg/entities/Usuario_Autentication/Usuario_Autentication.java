package com.bank.sg.entities.Usuario_Autentication;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "usuario_autentication")
public class Usuario_Autentication {
    @Id
    @SequenceGenerator(name="usuario_autentication_sequence",sequenceName = "usuario_autentication_sequence",allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="sequence_generator")
    private Integer id;
}
