package com.bank.sg.entities.Sucursal;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "sucursal")
@Data
public class Sucursal {
    @Id
    @SequenceGenerator(name="sucursal_sequence",sequenceName = "sucursal_sequence",allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="sequence_generator")
    private Integer id;

}
