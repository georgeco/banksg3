package com.bank.sg.entities.Documento_personal;

import javax.persistence.*;

@Entity
@Table(name = "documento_personal")
public class Documento_Personal {
    @Id
    @SequenceGenerator(name="documento_personal_sequence",sequenceName = "documento_personal_sequence",allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="sequence_generator")
    private Integer id;

    @OneToOne()
    private Tipo_documento tipo_documento;
    private String numero;



}
