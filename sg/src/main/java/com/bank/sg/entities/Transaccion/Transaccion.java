package com.bank.sg.entities.Transaccion;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "transaccion")
public class Transaccion {
    @Id
    @SequenceGenerator(name="transaccion_sequence",sequenceName = "transaccion_sequence",allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="sequence_generator")
    private Integer id;
}
