package com.bank.sg.entities.cuenta;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "cuenta_inversion")
@Data
public class Cuenta_Inversion {
    @Id
    @SequenceGenerator(name="cuenta_sequence",sequenceName = "cuenta_sequence",allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="sequence_generator")
    private Integer id;

    private String nombre;
    private Double interes_fijo;
    private Integer tiempo_fijo;




}
