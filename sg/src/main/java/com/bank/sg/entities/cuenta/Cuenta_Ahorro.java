package com.bank.sg.entities.cuenta;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "cuenta_ahorro")
public class Cuenta_Ahorro {
    @Id
    @SequenceGenerator(name="cuenta_ahorro_sequence",sequenceName = "cuenta_ahorro_sequence",allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="sequence_generator")
    private Integer id;

    private String nombre;
    private String apellido;
    private Double interes;
    private Double saldo_maximo;
    private Double saldo_minimo;
    private Double tope_maximo_cuenta;
}
