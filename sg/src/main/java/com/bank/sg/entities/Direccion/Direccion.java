package com.bank.sg.entities.Direccion;


import org.hibernate.annotations.Generated;

import javax.persistence.*;

@Entity
@Table(name = "direccion")
public class Direccion {
    @Id
    @SequenceGenerator(name="direccion_sequence",sequenceName = "direccion_sequence",allocationSize = 1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence_generator")
    private Integer id;

    //@OneToOne(mappedBy = "direccion")
    //private Ciudad ciudad;

    //@OneToOne(mappedBy = "direccion")
    //private Tipo_via tipo_via;

    private Integer numero_via_principal;
    private Integer nomenclatura_principal;
    private String prefijo;

    private String complemento;

}
