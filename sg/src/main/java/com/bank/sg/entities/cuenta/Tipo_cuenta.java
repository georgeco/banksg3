package com.bank.sg.entities.cuenta;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "tipo_cuenta")
@Data
public class Tipo_cuenta {
    @Id
    @SequenceGenerator(name="tipo_cuenta_sequence",sequenceName = "tipo_cuenta_sequence",allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="sequence_generator")
    private Integer id;

    @OneToOne()
    private Cuenta_Inversion cuenta_inversion;

    @OneToOne()
    private Cuenta_Ahorro cuenta_ahorro;

}
