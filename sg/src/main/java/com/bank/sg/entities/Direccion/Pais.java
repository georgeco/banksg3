package com.bank.sg.entities.Direccion;

import javax.persistence.*;

@Entity
@Table(name = "pais")
public class Pais {
    @Id
    @SequenceGenerator(name="pais_sequence",sequenceName = "pais_sequence",allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="sequence_generator")
    private Integer id;

    private String pais;
}
