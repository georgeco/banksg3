package com.bank.sg.dao.Direccion;

import com.bank.sg.entities.Direccion.Pais;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaisRespository extends JpaRepository<Pais,Integer> {
}
