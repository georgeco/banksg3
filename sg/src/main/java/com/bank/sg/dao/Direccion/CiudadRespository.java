package com.bank.sg.dao.Direccion;

import com.bank.sg.entities.Direccion.Ciudad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CiudadRespository extends JpaRepository<Ciudad, Integer> {
}
