package com.bank.sg.dao.DocumentoPersonal;

import com.bank.sg.entities.Documento_personal.Tipo_documento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoDocumentoRespository extends JpaRepository<Tipo_documento,Integer> {
}
