package com.bank.sg.dao.Transaccion;

import com.bank.sg.entities.Transaccion.Transaccion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransaccionRespository extends JpaRepository<Transaccion,Integer> {
}
