package com.bank.sg.dao.Direccion;

import com.bank.sg.entities.Direccion.Direccion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DireccionRespository extends JpaRepository<Direccion, Integer> {
}
