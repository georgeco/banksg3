package com.bank.sg.dao.Cuenta;

import com.bank.sg.entities.cuenta.Cuenta_Inversion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CuentaInversionRespository extends JpaRepository<Cuenta_Inversion,Integer> {
}
