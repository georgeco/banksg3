package com.bank.sg.dao.Sucursal;

import com.bank.sg.entities.Sucursal.Tipo_Sucursal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoSucursalRespository extends JpaRepository<Tipo_Sucursal,Integer> {
}
