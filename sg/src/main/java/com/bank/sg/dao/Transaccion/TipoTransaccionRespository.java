package com.bank.sg.dao.Transaccion;

import com.bank.sg.entities.Transaccion.Tipo_Transaccion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoTransaccionRespository extends JpaRepository<Tipo_Transaccion,Integer> {
}
