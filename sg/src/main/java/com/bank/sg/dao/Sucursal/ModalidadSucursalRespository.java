package com.bank.sg.dao.Sucursal;

import com.bank.sg.entities.Sucursal.Modalidad_Sucursal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModalidadSucursalRespository extends JpaRepository<Modalidad_Sucursal,Integer> {
}
