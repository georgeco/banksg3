package com.bank.sg.dao.Cuenta;

import com.bank.sg.entities.cuenta.Estado_Cuenta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoCuentaRespository extends JpaRepository<Estado_Cuenta,Integer> {
}
