package com.bank.sg.dao.Cuenta;

import com.bank.sg.entities.Sucursal.Sucursal;
import com.bank.sg.entities.cuenta.Cuenta_Ahorro;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CuentaAhorroRespository extends JpaRepository<Cuenta_Ahorro,Integer> {
}
