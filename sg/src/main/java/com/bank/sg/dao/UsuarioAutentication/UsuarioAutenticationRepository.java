package com.bank.sg.dao.UsuarioAutentication;

import com.bank.sg.entities.Usuario;
import com.bank.sg.entities.Usuario_Autentication.Usuario_Autentication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsuarioAutenticationRepository extends JpaRepository<Usuario_Autentication,Integer> {
    Optional<Usuario> findByUsername(String nombre);
    Boolean existByUsername(String nombre);
    Boolean existByEmail(String email);
}
