package com.bank.sg.dao.Cuenta;

import com.bank.sg.entities.cuenta.Cuenta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CuentaRespository extends JpaRepository<Cuenta,Integer> {
}
