package com.bank.sg.dao.DocumentoPersonal;

import com.bank.sg.entities.Documento_personal.Documento_Personal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentoPersonalRespository extends JpaRepository<Documento_Personal,Integer> {
}
